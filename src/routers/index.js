import SignIn from 'src/pages/signIn';

// Student
const menuItems = [
  {
    name: null,
    component: <SignIn />,
    layout: '',
    path: '/auth/sign-in',
    exact: true,
    isPrivate: false,
  },
];

export default menuItems;
