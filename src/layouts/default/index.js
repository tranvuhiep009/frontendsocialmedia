import React from 'react';
import classNames from 'classnames/bind';

// Component

// Other
import styles from './defaultLayout.module.scss';

const cx = classNames.bind(styles);

const DefaultLayout = () => {
  return (
    // <div className={cx('defaultLayout')}>
    <div className='bg-blue-500 text-white p-4'>Hello, Tailwind CSS!</div>
    // </div>
  );
};

export default DefaultLayout;
